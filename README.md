# ARDUINO ETHERNET WEB SERVER

The purpose of this page is to explain the implementation of an Ethernet WEB server able to display HighCharts graphs.

Depending on the board used, it's possible to use HTML templates, and store them in FLASH memory or SDCARD.

The project uses the following components :

 * an ARDUINO UNO or MEGA
 * a W5100 Ethernet board
 
### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/02/un-web-server-sur-ethernet.html
