
#ifndef _HTTP_H_
#define _HTTP_H_

// USE_TEMPLATE & USE_TEMPLATE_FILE only work on MEGA
// define to 0 for UNO or NANO boards
#define USE_TEMPLATE          0
#define USE_TEMPLATE_FILE     0

#include <Ethernet.h>
#if USE_TEMPLATE_FILE
#include <SdFat.h>
#endif
#include <sdios.h>

#define REQUEST_MAX           150
#define URL_MAX               80
#define ARGSTR_MAX            100
#define ARG_MAX               5
#define HANDLER_MAX           10
#define NAMESPACE_MAX         10
#define NAMESPACE_NAME_MAX    20
#define NAMESPACE_DATA_MAX    20

//#define DEBUG  // uncomment to debug
#ifdef DEBUG
#define trace     Serial.print
#define traceln   Serial.println
#else
#define trace
#define traceln
#endif

class http_request;

struct cgi_arg
{
  const char *name;
  const char *value;
};

struct url_handler
{
  const char *url;
  void (*func)(EthernetClient *client, http_request *req);
};

struct name_space
{
  const char *name;
  int (*get)(int index, char *data);
};

class http_request
{
  public:
    http_request(void);
    int read(EthernetClient *client);
    void setHandler(const char *url, void (*handler)(EthernetClient * client, http_request *req));
    int handle(EthernetClient *client, char *request, char *argStr);
    const char *getMethod(void) {
      return _type;
    }
    const char *getArgName(int n) {
      return _args[n].name;
    }
    const char *getArgValue(const char *name);
    const char *getHttpVersion(void) {
      return _proto;
    }
    const char *getIp(void) {
      return _ip;
    }
    const char *getUserAgent(void);
    const char *getFullURL(void) {
      return _fullUrl;
    }
    const char *getURL(void) {
      return _url;
    }
    void setFavIcon(const __FlashStringHelper *icon);
    void sendHeader(EthernetClient *client, const char *title, int refresh);
    void startBody(EthernetClient *client);
    void stopBody(EthernetClient *client);
    void resetNameSpace();
    void addNameSpace(const char *name, int (*get)(int index, char *data));
    void sendDataFromTemplate(EthernetClient *client, const char *data);
#if USE_TEMPLATE_FILE
    void sendDataFromTemplateFile(EthernetClient *client, SdFat *sd, const char *filename);
#endif
    const char *decode(const char *src, char *dest, size_t size);

  private:
    static const __FlashStringHelper  *_icon;
    char *_type;
    char _fullUrl[URL_MAX];
    char *_url;
    char *_proto;
    char *_ip;
    char *_agent;
    struct cgi_arg _args[ARG_MAX];
    url_handler _handlers[HANDLER_MAX];
    struct name_space _name_spaces[NAMESPACE_MAX];
    void parse(char *request, char *argStr);
    struct url_handler *getHandler(const char *url);
    struct name_space *getNameSpace(const char *name);
    void handleNotFound(EthernetClient *client, const char *url);
    unsigned char h2int(char c);
};

#endif
