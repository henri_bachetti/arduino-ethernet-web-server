
#include <Arduino.h>
#include "http.h"

#define FORM "<form action=\"/form-handle\" method=\"post\">" \
  "<br />" \
  "<label for=\"name\">Name : </label>" \
  "<br />" \
  "<input type=\"text\" id=\"name\" name=\"user_name\">" \
  "<br />" \
  "<label for=\"mail\">email : </label>" \
  "<br />" \
  "<input type=\"email\" id=\"mail\" name=\"user_mail\">" \
  "<br />" \
  "<label for=\"msg\">Message : </label>" \
  "<br />" \
  "<textarea id=\"msg\" name=\"user_message\"></textarea>" \
  "<br />" \
  "<input type=\"submit\" value=\"Send\">" \
  "</form>"

#if USE_TEMPLATE

const char PROGMEM tempHumTemplate[] = "<script src = \"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>\n"
"<script src=\"https://code.highcharts.com/highcharts.js\"></script>\n"
"<script type=\"text/javascript\">\n"
"jQuery(document).ready(function() {\n"
"  var optionsTemp = {\n"
"    chart: {\n"
"      renderTo: 'containerTemp',\n"
"      type: 'line'\n"
"    },\n"
"    title: {\n"
"      text: 'Temperature'\n"
"    },\n"
"    xAxis: {\n"
"      categories: [$(TEMP_CATEGORIES)]\n"
"    },\n"
"    yAxis: {\n"
"      title: {\n"
"        text: 'Temperature (°C)'\n"
"      }\n"
"    },\n"
"    series: [{\n"
"      name: 'Temperature',\n"
"      data: [$(TEMP_DATA)]\n"
"    }]\n"
"  };\n"
"  var optionsHum = {\n"
"    chart: {\n"
"      renderTo: 'containerHum',\n"
"      type: 'line'\n"
"    },\n"
"    title: {\n"
"      text: 'Humidity'\n"
"    },\n"
"    xAxis: {\n"
"      categories: [$(HUM_CATEGORIES)]\n"
"    },\n"
"    yAxis: {\n"
"      title: {\n"
"        text: 'Humidity (%)'\n"
"      }\n"
"    },\n"
"    series: [{\n"
"      name: 'Humidity',\n"
"      data: [$(HUM_DATA)]\n"
"    }]\n"
"  };\n"
"  var chartTemp = new Highcharts.Chart(optionsTemp);\n"
"  var chartHum = new Highcharts.Chart(optionsHum);\n"
"});</script>\n"
"<div id=\"containerTemp\" style=\"width:100%; height:400px;\"></div>\n"
"<div id=\"containerHum\" style=\"width:100%; height:400px;\"></div>\n";

#endif
