
#include "http.h"

#define FAVICON "AAABAAEAEBAAAAAAAABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJuWADKgmgAAm5UAFpqVAAablgAsnJYASpmUAAKblgAnm5UAKJuWAD6clgBKm5YAJZqUAASalQAJm5YAXZyXAACalQAXm5YAo1xmAACblgDrm5UAGpuWAHablgD5m5YA8ZuWAOCblgAAm5YAfpuWAIOblgDnm5UAPZuWAAeblgCam5UAAJuWAP+TkAACm5YA4ZuWADWblgDtm5YAiJuWAPGblgDgm5YAv5uWAPKblgB+mZUAEJqVAB+blgDgm5YAMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAm5YAJZuWAP+clwD/nJcA/5uWAP6blgAiAAAAAAAAAACblgAbm5YA/pyXAP+clwD/m5YA/5uWAC4AAAAAm5UAGZyXAP+blgDVmpQAC5qVAA2blgDZnJcA/5uWAFGblgBCnJcA/5uWAOGalAAQmpUACpuWAMyclwD/m5YAJZuWAPublgDmmZUAAAAAAAAAAAAAAAAAAJuWAJKclwD/nJcA/5uWAKackwAAm5UANpqUABWZlQAAm5YA25uWAP6clwD/m5YAQZuXAACblwD5m5cA+ZqXAAaZlAAAm5YA/5uWAP+alQAAmpcABpuWAPublgD6nZgAAJuWACyclwD/nJcA/5uWAJ8AAAAAAAAAAAAAAAAAAAAAm5YAGpyXAP+clwD/mpYAIgAAAACblgBcm5UAJAAAAACblgCLnJcA/5uWAHmclwD/m5YALAAAAAAAAAAAm5YAL5yXAP+blgDXm5YAy5yXAP+blQA3AAAAAAAAAACblQAknJcA/5uWAIyZlQAAm5YAxpyXAP+clwD/nJcA/5yXAP+blgDHmpQAAJqXAACblgC7nJcA/5yXAP+clwD/nJcA/5uWANGakQAGAAAAAAAAAACYkgACm5YAVZuWAFGYlAABAAAAAAAAAAAAAAAAAAAAAJeUAACblgBOm5YAWJmSAAOZjwAAm5MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//8AAP//AAD//wAArGYAAKgdAAD//wAAw8MAAJmZAAA8PAAAZmYAAD58AAC8PAAAgYEAAP//AAD//wAA//8AAA=="

const __FlashStringHelper *http_request::_icon;

// constructeur
http_request::http_request(void)
{
  memset(_handlers, 0, sizeof(_handlers));
  memset(_args, 0, sizeof(_args));
  memset(_name_spaces, 0, sizeof(_name_spaces));
  _icon = F(FAVICON);
}

// lit la prochaine requête et l'exécute
int http_request::read(EthernetClient *client)
{
  static char request[REQUEST_MAX + 1];
  int reqIndex = 0;
  traceln(F("new client"));
  // an http request ends with a blank line
  boolean blankLine = true;
  while (client->connected()) {
    if (client->available()) {
      char c = client->read();
      if (reqIndex < REQUEST_MAX) {
        request[reqIndex++] = c;
        request[reqIndex] = 0;
        traceln(request);
      }
      if (c == '\n' && blankLine && !strncmp(request, "GET", 3)) {
        traceln(request);
        handle(client, request, 0);
        break;
      }
      if (c == '\n' && blankLine && !strncmp(request, "POST", 4)) {
        char argStr[ARGSTR_MAX + 1];
        int i = 0;
        while (c > 0) {
          c = client->read();
          if (c > 0 && i < ARGSTR_MAX) {
            argStr[i++] = c;
            argStr[i] = 0;
          }
          else {
            Serial.println(F("buffer is OVER"));
          }
        }
        handle(client, request, argStr);
        break;
      }
      if (c == '\n') {
        // you're starting a new line
        blankLine = true;
      } else if (c != '\r') {
        // you've gotten a character on the current line
        blankLine = false;
      }
    }
  }
  // give the web browser time to receive the data
  delay(1);
  // close the connection:
  client->stop();
  traceln(F("client disconnected"));
  return reqIndex;
}

// enregistre une fonction handler
void http_request::setHandler(const char *url, void (*handler)(EthernetClient * client, http_request *req))
{
  trace(F("Set handler: "));
  traceln(url);
  for (int i = 0 ; i < URL_MAX ; i++) {
    if (_handlers[i].url == 0) {
      _handlers[i].url = url;
      _handlers[i].func = handler;
      return;
    }
  }
}

// exécute le handler associé à une une URL
int http_request::handle(EthernetClient *client, char *request, char *argStr)
{
  parse(request, argStr);
  struct url_handler *handler = getHandler(_url);
  if (handler == 0) {
    handleNotFound(client, _url);
    return -1;
  }
  for (int i = 0 ; i < ARG_MAX ; i++) {
    if (_args[i].name) {
      trace(F("ARG: ")); trace(_args[i].name); trace(F("=")); traceln(_args[i].value);
    }
  }
  handler->func(client, this);
  return 0;
}

// retourne la valeur d'un argument
const char *http_request::getArgValue(const char *name)
{
  trace(F("Get arg: "));
  trace(name);
  trace(F(" : "));
  for (int i = 0 ; i < ARG_MAX ; i++) {
    if (_args[i].name) {
      if (!strcmp(_args[i].name, name)) {
        traceln(_args[i].value);
        return _args[i].value;
      }
    }
  }
  traceln(F("None"));
  return 0;
}

// retourne la version du navigateur
const char *http_request::getUserAgent(void)
{
  return _agent;
}

void http_request::setFavIcon(const __FlashStringHelper *icon)
{
  _icon = icon;
}

// envoie un header HTML
// title: titre de la page
// refresh: demande un rafraichissement de la page au navigateur (en secondes)
void http_request::sendHeader(EthernetClient *client, const char *title, int refresh)
{
  client->println(F("HTTP/1.1 200 OK\nContent-Type: text/html\nConnection: close"));
  if (refresh) {
    client->print(F("Refresh:"));
    client->println(refresh);
  }
  client->println();
  client->println(F("<!DOCTYPE HTML>"));
  client->println("<html><head>");
  client->println(F("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"));
  client->print(F("<link href=\"data:image/x-icon;base64,"));
  client->print(_icon);
  client->println(F("\" rel=\"icon\" type=\"image/x-icon\" />"));
  client->println("<title>");
  client->println(title);
  client->println("</title>");
  client->println("</head>");
}

// envoie le début du <body>
void http_request::startBody(EthernetClient *client)
{
  client->println(F("<body>"));
}

// envoie la fin du <body>
void http_request::stopBody(EthernetClient *client)
{
  client->println(F("</body></html>"));
}

void http_request::resetNameSpace(void)
{
  memset(_name_spaces, 0, sizeof(_name_spaces));
}

void http_request::addNameSpace(const char *name, int (*get)(int index, char *data))
{
  trace(F("http_request::addNameSpace: "));

  for (int i = 0 ; i < NAMESPACE_MAX ; i++) {
    if (_name_spaces[i].name == 0) {
      _name_spaces[i].name = name;
      _name_spaces[i].get = get;
      traceln(i);
      return;
    }
  }
}

#if USE_TEMPLATE

void http_request::sendDataFromTemplate(EthernetClient *client, const char *data)
{
  int i = 0;

  traceln(F("http_request::sendDataFromTemplate: "));
  byte c = pgm_read_byte(data + i);
  while (c != 0) {
    client->print(char(c));
    c = pgm_read_byte(data + ++i);
    if (c == '$') {
      char name[NAMESPACE_NAME_MAX];
      int n = 0;
      c = pgm_read_byte(data + ++i);
      if (c == '(') {
        while (c != ')') {
          c = pgm_read_byte(data + ++i);
          if (c != ')') {
            if (n < NAMESPACE_NAME_MAX - 1) {
              name[n++] = c;
            }
            else {
              traceln(F("name too long"));
            }
          }
        }
        c = pgm_read_byte(data + ++i);
        name[n] = '\0';
        traceln(name);
        struct name_space *ns = getNameSpace(name);
        if (ns == 0) {
          traceln(F("namespace not found"));
        }
        else {
          int index = 0;
          char data[NAMESPACE_DATA_MAX];
          while (ns->get(index, data) != -1) {
            if (index != 0) {
              client->print(", ");
            }
            traceln(data);
            client->print(data);
            index++;
          }
        }
      }
      else {
        client->print('$');
        client->print(char(c));
      }
    }
  }
  traceln(F("OK"));
}

#endif

#if USE_TEMPLATE_FILE

void http_request::sendDataFromTemplateFile(EthernetClient *client, SdFat *sd, const char *filename)
{
  int i = 0;
  byte c;
  File fd;

  if (!sd->exists(filename)) {
    Serial.println(F("template.txt not found"));
    return;
  }
  fd = sd->open(filename);
  traceln(F("http_request::sendDataFromTemplateFile: "));
  if (fd.available()) {
    c = fd.read();
  }
  while (fd.available()) {
    client->print(char(c));
    c = fd.read();
    if (c == '$') {
      char name[NAMESPACE_NAME_MAX];
      int n = 0;
      c = fd.read();
      if (c == '(') {
        while (c != ')') {
          c = fd.read();
          if (c != ')') {
            if (n < NAMESPACE_NAME_MAX - 1) {
              name[n++] = c;
            }
            else {
              traceln(F("name too long"));
            }
          }
        }
        c = fd.read();
        name[n] = '\0';
        traceln(name);
        struct name_space *ns = getNameSpace(name);
        if (ns == 0) {
          traceln(F("namespace not found"));
        }
        else {
          int index = 0;
          char data[NAMESPACE_DATA_MAX];
          while (ns->get(index, data) != -1) {
            if (index != 0) {
              client->print(", ");
            }
            traceln(data);
            client->print(data);
            index++;
          }
        }
      }
      else {
        client->print('$');
        client->print(char(c));
      }
    }
  }
  fd.close();
  traceln(F("OK"));
}

#endif

// analyse l'URL
void http_request::parse(char *request, char *argStr)
{
  char *urlStart;

  trace(F("http_request::parse: "));
  traceln(request);
  char *token = strtok(request, " ");
  if (token != NULL) {
    _type = token;
  }
  token = strtok(NULL, " ");
  if (token != NULL) {
    urlStart = token;
    strcpy(_fullUrl, token);
  }
  token = strtok(NULL, "/");
  token = strtok(NULL, "\n");
  if (token != NULL) {
    _proto = token;
  }
  token = strtok(NULL, " ");
  token = strtok(NULL, "\n");
  if (token != NULL) {
    _ip = token;
  }
  token = strtok(NULL, " ");
  token = strtok(NULL, "\n");
  if (token != NULL) {
    _agent = token;
  }
  token = strtok(urlStart, "?");
  if (token != NULL) {
    _url = token;
  }
  memset(_args, 0, sizeof(_args));
  if (argStr) {
    token = strtok(argStr, "=");
  }
  else {
    token = strtok(NULL, "=");
  }
  int i = 0;
  while (i < ARG_MAX && token != NULL) {
    _args[i].name = token;
    token = strtok(NULL, "&");
    if (token != NULL) {
      _args[i++].value = token;
    }
    token = strtok(NULL, "=");
  }
  trace(F("Type: ")); traceln(_type);
  trace(F("URL: ")); traceln(_url);
  for (int i = 0 ; i < ARG_MAX ; i++) {
    if (_args[i].name) {
      trace(F("ARG: ")); trace(_args[i].name); trace(F("=")); traceln(_args[i].value);
    }
  }
  trace(F("HTTP: ")); traceln(_proto);
  trace(F("IP: ")); traceln(_ip);
  trace(F("Agent: ")); traceln(_agent);
}

// récupère le handler correspondant à une URL
struct url_handler *http_request::getHandler(const char *url)
{
  trace(F("Search for handler: "));
  traceln(url);
  for (int i = 0 ; i < URL_MAX ; i++) {
    if (_handlers[i].url != 0) {
      if (!strcmp(_handlers[i].url, url)) {
        trace(i); trace(F(": ")); traceln(_handlers[i].url);
        return &_handlers[i];
      }
    }
  }
  traceln(F("Handler not found"));
  return 0;
}

struct name_space *http_request::getNameSpace(const char *name)
{
  for (int i = 0 ; i < NAMESPACE_MAX ; i++) {
    if (!strcmp(_name_spaces[i].name, name)) {
      return &_name_spaces[i];
    }
  }
  return 0;
}

// gère les URLs invalides
void http_request::handleNotFound(EthernetClient *client, const char *url)
{
  IPAddress addr = Ethernet.localIP();
  char ip[16];

  traceln(F("http_request::handleNotFound"));
  for (int i = 0; i < 4 ; i++) {
    sprintf(ip, "%d.%d.%d.%d", addr[0], addr[1], addr[2], addr[3]);
  }
  const char *htmlHeader = "<!DOCTYPE HTML PUBLIC " - //IETF//DTD HTML 2.0//EN">\n<html><head>\n<title>404 Not Found</title>\n</head><body>\n";
                     client->println(htmlHeader);
  client->println("<h1>Not Found</h1>\n<p>The requested URL ");
  client->print(url);
  client->println(" was not found on this server.</p>\n<hr>\n<address>Arduino Server at ");
  client->print(ip); client->print(" Port "); client->print(client->localPort());
  client->print("</address>\n</body></html>");
}

unsigned char http_request::h2int(char c)
{
  if (c >= '0' && c <= '9') return ((unsigned char)c - '0');
  if (c >= 'a' && c <= 'f') return ((unsigned char)c - 'a' + 10);
  if (c >= 'A' && c <= 'F') return ((unsigned char)c - 'A' + 10);
  return (0);
}

/*
   decode escaped chars like %20, etc
*/
const char *http_request::decode(const char *src, char *dest, size_t size)
{
  int i = 0;
  char code0;
  char code1;

  while (*src++) {
    if (*src == '%' && *(src + 1) != 0 && *(src + 2) != 0) {
      src++;
      code0 = *src;
      src++;
      code1 = *src;
      if (i < size) {
        dest[i++] = (h2int(code0) << 4) | h2int(code1);
      }
    }
    else {
      if (i < size) {
        dest[i++] = *src;
      }
    }
  }
  dest[i] = '\0';
  return dest;
}
