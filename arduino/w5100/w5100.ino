
// ARDUINO ETHERNET WEB SERVER
// The purpose of this project is to explain the implementation of an Ethernet WEB server able to display HighCharts graphs.
// another library is used (debug) :  https://bitbucket.org/henri_bachetti/mpp-console-logger.git

#include <SPI.h>
#include "http.h"

#include "pages.cpp"

#define USE_FORM              1
#define USE_GRAPHICS          1

#define SPI_SPEED SD_SCK_MHZ(50)

// adresse et port du serveur
#define IP_ADDR   "192.168.1.177"
#define PORT      80
#define FAV_ICON "AAABAAEAEBAAAAAAAABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AJCQk/yQkJP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/0ZGRv8XFxf/////AP///wD///8A////AP///////////////+7u7v9OTk7/sbGx/11dXf9KSkr///////////+lpaX/4eHh/////wD///8A////AAAAAP8AAAD///////////+8vLz///////X19f/t7e3/gICA/5eXl////////////wAAAP8AAAD/////AKysrP8AAAD/AAAA/wAAAP+enp7/////////////////////////////////5OTk/wAAAP8AAAD/AAAA/6ysrP////8A////AP///wCurq6PlJSUzP///wD///8A////AP///wD///8A////AJSUlMyQkJDY////AP///wD///8A////AP///wD///8AAAAA/wAAAP////8A////AP///wD///8A////AP///wAAAAD/AAAA/////wD///8A////AP///wD///8AAAAA/wAAAP8AAAD/////AP///wD///8A////AP///wD///8AAAAA/wAAAP8AAAD/////AP///wD///8ACwsL/wAAAP8AAAD/lJSU2AAAAP8AAAD/cnJy/1hYWP8AAAD/AAAA/2FhYdgHBwf/AAAA/wAAAP////8A////AP///wD///8A////AP///wAAAAD/s7Oz/wAAAP8AAAD/x8fH/wAAAP////8A////AP///wD///8A////AP///wD///8A////AP///wAAAAD/AAAA/////wAAAAD/AAAA/////wAAAAD/AAAA/////wD///8A////AP///wD///8A////AP///wD///8AAAAA/wAAAP+qqqqlAAAA/wAAAP9lZWXjAAAA/wAAAP////8A////AP///wD///8A////AP///wD///8A////ALa2tv////8A9vb2UQAAAP8AAAD/////Uf///wDAwMD/////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wBcXFz/AAAA/////wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A//8AAP//AADAAwAAwAMAAIABAAAAAAAA5+cAAOfnAADH4wAAgAEAAPgfAADyTwAA8A8AAPZvAAD+fwAA//8AAA=="

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip;

EthernetServer server(PORT);
http_request http;

struct temperature
{
  const char *time;
  float temp;
  uint8_t hum;
};

#if USE_TEMPLATE_FILE
SdFat sd;
#endif

// les données du serveur
struct temperature temperatures[] = {{"07:00", 20.1, 35}, {"08:00", 20.0, 35}, {"09:00", 19.8, 34},
  {"10:00", 20.1, 36}, {"11:00", 20.05, 35}, {"12:00", 20.20, 36},
  {"13:00", 20.3, 35}, {"14:00", 20.1, 34}, {"15:00", 19.8, 33},
  {"16:00", 20.0, 35}, {"17:00", 20.2, 36}, {"18:00", 20.1, 36}
};

const int temperatureSize = (sizeof(temperatures) / sizeof(struct temperature));

void setup() {
  Serial.begin(115200);
  Serial.println(F("Ethernet WebServer Example"));
  ip.fromString(IP_ADDR);
  Ethernet.begin(mac, ip);
  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println(F("Ethernet shield was not found.  Sorry, can't run without hardware. :("));
    while (true) {
      delay(1); // do nothing, no point running without Ethernet hardware
    }
  }
  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println(F("Ethernet cable is not connected."));
  }
  // démarre le server
  server.begin();
  Serial.print(F("server is at "));
  Serial.println(Ethernet.localIP());
#if USE_TEMPLATE_FILE
  if (!sd.begin(4, SPI_SPEED)) {
    if (sd.card()->errorCode()) {
      Serial.println("SD initialization failed");
    }
  }
  if (sd.exists("template.txt")) {
    Serial.println(F("template.txt found OK"));
  }
#endif
  // handler pour l'URL /
  http.setHandler("/", handleRoot);
  // handler pour l'URL /parse
  http.setHandler("/parse", handleParse);
  // handler pour l'URL /form
  http.setHandler("/form", handleForm);
  // handler pour l'URL /form-
  http.setHandler("/form-handle", handleFormResult);
  // handler pour l'URL /temp
  http.setHandler("/temp", handleTemp);
  // handler pour l'URL /hum
  http.setHandler("/hum", handleHumidity);
#if USE_GRAPHICS
  // handler pour l'URL /graphics
  http.setHandler("/graphics", handleGraphics);
#endif
#if USE_TEMPLATE
  // handler pour l'URL /template
  http.setHandler("/template", handleTemplate);
#endif
#if USE_TEMPLATE_FILE
  // handler pour l'URL /template-file
  http.setHandler("/template-file", handleTemplateFile);
#endif
  http.setFavIcon(F(FAV_ICON));
}

//#################### HOME PAGE ####################

// page d'accueil
void handleRoot(EthernetClient *client, http_request *req)
{
  Serial.println(F("handleRoot"));
  http.sendHeader(client, "Arduino Server", 0);
  http.startBody(client);
  client->print(F("<h1>Welcome to the Arduino Server</h1>\n"
                  "<input type=\"button\" onclick=\"location.href='/parse?arg1=1234&arg2=azerty&arg3=5678';\""
                  "value=\"Parse an URL\" style=\"margin:5px; color:darkblue; border-color:blue;\"/>"
#if USE_FORM
                  "<input type=\"button\" onclick=\"location.href='/form';\""
                  "value=\"Form\" style=\"margin:5px; color:darkblue; border-color:blue;\"/>"
#endif
                  "<input type=\"button\" onclick=\"location.href='/temp';\""
                  "value=\"Go to temperature page\" style=\"margin:5px; color:darkblue; border-color:blue;\"/>"
                  "<input type=\"button\" onclick=\"location.href='/hum';\""
                  "value=\"Go to humidity page\" style=\"margin:5px; color:darkblue; border-color:blue;\"/>"
#if USE_GRAPHICS
                  "<input type=\"button\" onclick=\"location.href='/graphics';\""
                  "value=\"Display graphics\" style=\"margin:5px; color:darkblue; border-color:blue;\"/>"
#endif
#if USE_TEMPLATE
                  "<input type=\"button\" onclick=\"location.href='/template';\""
                  "value=\"Display template\" style=\"margin:5px; color:darkblue; border-color:blue;\"/>"
#endif
#if USE_TEMPLATE_FILE
                  "<input type=\"button\" onclick=\"location.href='/template-file';\""
                  "value=\"Display template file\" style=\"margin:5px; color:darkblue; border-color:blue;\"/>"
#endif
                  "<br />"
                  "<input type=\"button\" onclick=\"location.href='/temp?hour=10:00';\""
                  "value=\"Temperature at 10:00\" style=\"margin:5px; color:darkblue; border-color:blue;\"/>"
                  "<input type=\"button\" onclick=\"location.href='/hum?hour=10:00';\""
                  "value=\"Humidity at 10:00\" style=\"margin:5px; color:darkblue; border-color:blue;\"/>"
                 ));
  http.stopBody(client);
}

void handleParse(EthernetClient *client, http_request *req)
{
  Serial.println(F("handleParse"));
  http.sendHeader(client, "Arduino Server", 0);
  http.startBody(client);
  client->print(F("Type: ")); client->print(http.getMethod()); client->println(F("<br />"));
  client->print(F("Full URL: ")); client->print(http.getFullURL()); client->println(F("<br />"));
  client->print(F("URL: ")); client->print(http.getURL()); client->println(F("<br />"));
  client->print(F("PROTO: ")); client->print(http.getHttpVersion()); client->println(F("<br />"));
  client->print(F("IP: ")); client->print(http.getIp()); client->println(F("<br />"));
  client->print(F("AGENT: ")); client->print(http.getUserAgent()); client->println(F("<br />"));
  client->println(F("<br />"));
  for (int i = 0 ; i < ARG_MAX ; i++) {
    const char *name = http.getArgName(i);
    if (name) {
      const char *value = http.getArgValue(name);
      client->print(name); client->print(F("=")); client->print(value);
      client->println(F("<br />"));
    }
  }
  http.stopBody(client);
}

#if USE_FORM

void handleForm(EthernetClient *client, http_request *req)
{
  Serial.println(F("handleForm"));
  http.sendHeader(client, "Arduino Server", 0);
  http.startBody(client);
  client->print(F(FORM));
  http.stopBody(client);
}

void handleFormResult(EthernetClient *client, http_request *req)
{
  char buf[50];

  Serial.println(F("handleFormResult"));
  http.sendHeader(client, "Arduino Server", 0);
  http.startBody(client);
  client->print("user_name"); client->print(F("=")); client->print(http.getArgValue("user_name"));
  client->println(F("<br />"));
  // decode the name%40server to name@server
  client->print("user_mail"); client->print(F("=")); client->print(http.decode(http.getArgValue("user_mail"), buf, 50));
  client->println(F("<br />"));
  client->print("user_message"); client->print(F("=")); client->print(http.getArgValue("user_message"));
  client->println(F("<br />"));
  http.stopBody(client);
}

#endif

//#################### TEMPERATURE PAGE ####################

void sendTemp(EthernetClient *client, struct temperature *temp)
{
  client->print("temperature at ");
  client->print(temp->time);
  client->print(" : ");
  client->print(temp->temp);
  client->println("&degC");
  client->println("<br />");
}

// envoie les températures au client
void handleTemp(EthernetClient *client, http_request *req)
{
  Serial.println(F("handleTemp"));
  http.sendHeader(client, "Temperature", 10);
  http.startBody(client);
  const char *hour = req->getArgValue("hour");
  for (int i = 0; i < temperatureSize ; i++) {
    if (hour) {
      if (!strcmp(temperatures[i].time, hour)) {
        sendTemp(client, &temperatures[i]);
      }
    }
    else {
      sendTemp(client, &temperatures[i]);
    }
  }
  http.stopBody(client);
}

//#################### HUMIDITY PAGE ####################

void sendHum(EthernetClient *client, struct temperature *temp)
{
  client->print("Humidity at ");
  client->print(temp->time);
  client->print(" : ");
  client->print(temp->hum);
  client->print("&#37;");
  client->println("<br />");
}

// envoie l'humidité au client
void handleHumidity(EthernetClient * client, http_request * req)
{
  Serial.println(F("handleHum"));
  http.sendHeader(client, "Humidity", 10);
  http.startBody(client);
  const char *hour = req->getArgValue("hour");
  for (int i = 0; i < temperatureSize ; i++) {
    if (hour) {
      if (!strcmp(temperatures[i].time, hour)) {
        sendHum(client, &temperatures[i]);
      }
    }
    else {
      sendHum(client, &temperatures[i]);
    }
  }
  http.stopBody(client);
}

//#################### GRAPHICS PAGE ####################

// envoie la temperature et l'humidité au client
void handleGraphics(EthernetClient * client, http_request * req)
{
  Serial.println(F("handleGraphics"));
  http.sendHeader(client, "Graphics", 0);
  http.startBody(client);
  client->print(F("<script src = \"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>\n"
                  "<script src=\"https://code.highcharts.com/highcharts.js\"></script>\n"));
  client->print(F("<script type=\"text/javascript\">\n"
                  "jQuery(document).ready(function() {\n"
                  "  var optionsTemp = {\n"
                  "    chart: {\n"
                  "      renderTo: 'containerTemp',\n"
                  "      type: 'line'\n"
                  "    },\n"
                  "    title: {\n"
                  "      text: 'Temperature'\n"
                  "    },\n"
                  "    xAxis: {\n"
                  "      categories: ["));
  for (int i = 0; i < temperatureSize ; i++) {
    if (i != 0) {
      client->print("', ");
    }
    client->print("'");
    client->print(temperatures[i].time);
  }
  client->print("']\n");
  client->print(F("    },\n"
                  "    yAxis: {\n"
                  "      title: {\n"
                  "        text: 'Temperature (°C)'\n"
                  "      }\n"
                  "    },\n"
                  "    series: [{\n"
                  "      name: 'Temperature',\n"
                  "      data: ["));
  for (int i = 0; i < temperatureSize ; i++) {
    if (i != 0) {
      client->print(", ");
    }
    client->print(temperatures[i].temp);
  }
  client->print("]\n");
  client->print(F("    }]\n"
                  "  };\n"
                  "  var optionsHum = {\n"
                  "    chart: {\n"
                  "      renderTo: 'containerHum',\n"
                  "      type: 'line'\n"
                  "    },\n"
                  "    title: {\n"
                  "      text: 'Humidity'\n"
                  "    },\n"
                  "    xAxis: {\n"
                  "      categories: ["));
  for (int i = 0; i < temperatureSize ; i++) {
    if (i != 0) {
      client->print("', ");
    }
    client->print("'");
    client->print(temperatures[i].time);
  }
  client->print("']\n");
  client->print(F("    },\n"
                  "    yAxis: {\n"
                  "      title: {\n"
                  "        text: 'Humidity (%)'\n"
                  "      }\n"
                  "    },\n"
                  "    series: [{\n"
                  "      name: 'Humidity',\n"
                  "      data: ["));
  for (int i = 0; i < temperatureSize ; i++) {
    if (i != 0) {
      client->print(", ");
    }
    client->print(temperatures[i].hum);
  }
  client->print("]\n");
  client->print(F("    }]\n"
                  "  };\n"
                  "  var chartTemp = new Highcharts.Chart(optionsTemp);\n"
                  "  var chartHum = new Highcharts.Chart(optionsHum);\n"
                  "});</script>\n"));
  client->println(F("<div id=\"containerTemp\" style=\"width:100%; height:400px;\"></div>"));
  client->println(F("<div id=\"containerHum\" style=\"width:100%; height:400px;\"></div>"));
  http.stopBody(client);
}

//#################### TEMPLATE PAGE ####################

int getX(int index, char *data)
{
  if (index >= temperatureSize) {
    return -1;
  }
  data[0] = '\'';
  strcpy(data + 1, temperatures[index].time);
  strcat(data, "'");
  return 0;
}

int getTempY(int index, char *data)
{
  if (index >= temperatureSize) {
    return -1;
  }
  dtostrf(temperatures[index].temp, 4, 2, data);
  return 0;
}

int getHumY(int index, char *data)
{
  if (index >= temperatureSize) {
    return -1;
  }
  itoa(temperatures[index].hum, data, 10);
  return 0;
}

#if USE_TEMPLATE

// envoie la temperature et l'humidité au client
void handleTemplate(EthernetClient * client, http_request * req)
{
  char tempData[200];
  Serial.println(F("handleTemplate"));
  http.sendHeader(client, "Template", 0);
  http.startBody(client);
  http.resetNameSpace();
  http.addNameSpace("TEMP_CATEGORIES", getX);
  http.addNameSpace("TEMP_DATA", getTempY);
  http.addNameSpace("HUM_CATEGORIES", getX);
  http.addNameSpace("HUM_DATA", getHumY);
  http.sendDataFromTemplate(client, tempHumTemplate);
  http.stopBody(client);
}

#endif

//#################### TEMPLATE FILE PAGE ####################

#if USE_TEMPLATE_FILE

// envoie la temperature et l'humidité au client
void handleTemplateFile(EthernetClient * client, http_request * req)
{
  char tempData[200];
  Serial.println(F("handleTemplate"));
  http.sendHeader(client, "Template", 0);
  http.startBody(client);
  http.resetNameSpace();
  http.addNameSpace("TEMP_CATEGORIES", getX);
  http.addNameSpace("TEMP_DATA", getTempY);
  http.addNameSpace("HUM_CATEGORIES", getX);
  http.addNameSpace("HUM_DATA", getHumY);
  http.sendDataFromTemplateFile(client, &sd, "template.txt");
  http.stopBody(client);
}

#endif

//#################### LOOP ####################

void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    // lit et exècute la prochaine requête
    http.read(&client);
  }
}
